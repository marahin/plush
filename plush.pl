#!/usr/bin/perl

use strict;
use warnings;
use utf8;

$|++; # 3lab.re

use Getopt::Long;
use Pod::Usage;
use Data::Dumper;

Getopt::Long::Configure ("bundling");

my $VERSION = '0.1a';

my $man = 0;
my $help = 0;
my $verbose = 0;

GetOptions(
    'help|?|h'    => \$help,
    'man'       => \$man,
    'verbose'   => \$verbose,
    'version|V' => \&version
    ) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitval => 0, -verbose => 2) if $man;


sub version {
    print(
'Plush '.$VERSION."
\"THE BEER-WARE\" LICENCE (Revision 42):
<xaxes\@3lab.re> and <marahin\@3lab.re> wrote this soft. As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy us a beer in return");
}

sub read_config {
    (my $config) = @_;
    my $re = '((?:[a-z][a-z]+)).*?((?:[a-z][a-z]+))';
    my %conf = ();

    for(split /^/, $config) {
        if($_ =~ m/$re/is) { $conf{$1} = $2; } }
    return \%conf;
}

__END__

=head1 PLUSH

Simple Perl script to deploy directory between servers

=head1 SYNOPSIS

plush [options] directory

Options:
  --help
  --man

=head1 OPTIONS

=over 8

=item B<-h, --help>

Prints help information and exits

=item B<--man>

Prints the manual page and exits

=item B<-v, --verbose>

Verbosely list files processed

=item B<-V, --version>

Prints version of this program

=back

=head1 DESCRIPTION

B<This program> will sync directory from slave servers definied in F<.plushrc> with the on on master server.

=cut
